package com.abedev.webacademy.homework2;

import android.support.annotation.ColorRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    private static final String BACK_STACK_KEY = "backstack";
    private final SparseArray<CheckBox> checkBoxByColor = new SparseArray<>();
    private Vector<Integer> backStack = new Vector<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpCheckBoxes();
    }
    private void setUpCheckBoxes() {
        setUpCheckBox(R.id.cb_red,R.color.redSemiTransparent);
        setUpCheckBox(R.id.cb_green,R.color.greenSemiTransparent);
        setUpCheckBox(R.id.cb_blue,R.color.blueSemiTransparent);
    }

    private void setUpCheckBox(@IdRes int id, @ColorRes final int color) {
        CheckBox checkBox = (CheckBox) findViewById(id);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked())
                    addColoredFragment(color);
                else
                    removeColoredFragment(color);
            }
        });
        checkBoxByColor.put(color,checkBox);
    }

    private void addColoredFragment(@ColorRes int color) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container,ColoredFragment.getInstance(color),String.valueOf(color))
                .commit();
        backStack.add(color);
    }

    private void removeColoredFragment(@ColorRes int color) {
        backStack.removeElement(color);
        getSupportFragmentManager()
                .beginTransaction()
                .remove(getSupportFragmentManager().findFragmentByTag(String.valueOf(color)))
                .commit();
    }


    @Override
    public void onBackPressed() {
        if (!backStack.isEmpty()) {
            int color = backStack.lastElement();
            removeColoredFragment(color);
            checkBoxByColor.get(color).setChecked(false);
        }
        else
            finish();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        fixFragmentsOrder();
    }

    //workaround for broken fragments order, android issue https://code.google.com/p/android/issues/detail?id=199531
    private void fixFragmentsOrder() {
        View[] rightOrder = new View[backStack.size()];
        for (int i = 0; i < backStack.size(); i++) {
            int color = backStack.elementAt(i);
            rightOrder[i] = getSupportFragmentManager().findFragmentByTag(String.valueOf(color)).getView();
        }
        ViewGroup container = (ViewGroup)findViewById(R.id.fragment_container);
        for (int i = 0; i < Math.min(container.getChildCount(),rightOrder.length); i++) {
            View childView  = container.getChildAt(i);
            if (!rightOrder[i].equals(childView)) {
                for (View view : rightOrder) {
                    container.bringChildToFront(view);
                }
                break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(BACK_STACK_KEY,backStack);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        backStack = new Vector<>((List<Integer>) savedInstanceState.getSerializable(BACK_STACK_KEY));
    }


}
