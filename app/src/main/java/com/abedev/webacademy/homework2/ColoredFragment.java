package com.abedev.webacademy.homework2;

import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * Created by home on 15.08.2016.
 */
public class ColoredFragment extends Fragment{
    private static final String ARG_COLOR = "color";
    @ColorRes
    private int color;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        color = getArguments().getInt(ARG_COLOR);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_colored,container,false);
        frameLayout.setBackgroundColor(ContextCompat.getColor(getContext(),color));
        return frameLayout;
    }

    public static ColoredFragment getInstance(@ColorRes int color) {
        ColoredFragment instance = new ColoredFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLOR,color);
        instance.setArguments(args);
        return instance;
    }
}
